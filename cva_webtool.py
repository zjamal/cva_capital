import os
import shutil
from BA_CVA.bacva_calculation import calculate_bacva
from BA_CVA.write_results_bacva import write_results_bacva
from SA_CVA.sacva_calculation import calculate_sacva
from SA_CVA.write_results_sacva import write_results_sacva
from flask import Flask, render_template, request, flash, send_from_directory, redirect
from werkzeug import secure_filename
import zipfile
import numpy as np

app = Flask(__name__)
curr_dir = os.path.abspath(os.path.dirname(__file__))
app.config['UPLOAD_FOLDER'] = os.path.join(curr_dir, 'web_uploads')
app.config['INPUT_FOLDER'] = os.path.join(app.config['UPLOAD_FOLDER'], 'input')
app.config['OUTPUT_FOLDER'] = os.path.join(curr_dir, 'web_results')
# app.config['NP_FOLDER'] = 'uploads/numpy/'
app.config['ALLOWED_EXTENSIONS'] = set(['csv'])

upload_dir = app.config['UPLOAD_FOLDER']
input_dir = app.config['INPUT_FOLDER']
result_dir = app.config['OUTPUT_FOLDER']


def zipdir(path, ziph):
    # ziph is zipfile handle
    for root, dirs, files in os.walk(path):
        for file in files:
            ziph.write(os.path.join(root, file))


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1] in app.config['ALLOWED_EXTENSIONS']


@app.route('/')
def index():
    return render_template('index.html')

# @app.route('/formulas')
# def FRTB_Formulas():
# return
# send_from_directory('','HowTo/FRTB_Risk_Charge_Calculation_Formulas.pdf')

# @app.route('/sampleinput')
# def FRTB_SampleInput():
#     return send_from_directory('','HowTo/Sample Input.zip')

# @app.route('/bucketmapping')
# def FRTB_BucketMapping():
#     return send_from_directory('','HowTo/SBM_Bucket_Mapping.pdf')


@app.route('/bacva', methods=['POST'])
def bacva():
    """Calculate bacva """
    # clean up existing input and output directories
    create_dir()

    # Save input files to uploads/input folder
    save_files()

    # Read input file paths
    ead_file, ctp_mapping_file, hedge_file = read_bacva_filepaths()

    # calculate bacva
    dict_df_data = calculate_bacva(ead_file, ctp_mapping_file, hedge_file)

    # write results to results folder
    result_file = os.path.join(result_dir, 'Summary.xls')
    write_results_bacva(dict_df_data, result_file)

    # zip the results
    ziph = zipfile.ZipFile(os.path.join(
        result_dir, 'results.zip'), 'w', zipfile.ZIP_DEFLATED)
    zipdir(result_dir, ziph)
    ziph.close()

    # send results to the webpage for download
    # it is sending the full folder path as zip. check syntax for this
    # function. Also display a popup saying calculating as it may take some
    # time
    return send_from_directory(result_dir, 'results.zip')

    # Go to the main page
    return redirect('/')



def create_dir():
    """Remove input and output directories if they exist and recreate them"""

    # remove it directory exists
    if os.path.exists(upload_dir):
        shutil.rmtree(upload_dir)
    if os.path.exists(result_dir):
        shutil.rmtree(result_dir)

    # create directory
    os.mkdir(upload_dir)
    os.mkdir(input_dir)
    os.mkdir(result_dir)


def save_files():
    """Save files to the uploads/input folder """

    input_file_list = ['ead_input', 'hedge_input', 'ctp_mapping_file'
                       'sensi_input', 'hedge_sensi_input', 'csr_bucket_mapping', 'com_bucket_mapping',
                       'csr_ref_mapping_file', 'csr_hedge_index_composition_file' ]

    for input_file in input_file_list:
        # Throws a bad key error when hedge file is missing. Debug this
        if input_file not in request.files:
            # flash('File not provided')
            pass
        else:
            file = request.files[input_file]
            if file and allowed_file(file.filename):
                filename = secure_filename(file.filename)
                filepath = os.path.join(input_dir, filename)
                try:
                    file.save(filepath)
                except:
                    print('File could not be uploaded')
                    return redirect('/')


def read_bacva_filepaths():
    upload_dir = app.config['UPLOAD_FOLDER']
    ead_file = os.path.join(upload_dir, 'input/ead_input.csv')
    # ead_file = os.path.join(curr_dir,'input/ead_input_sample.csv')
    ctp_mapping_file = os.path.join(upload_dir, 'input/cpty_mapping_file.csv')
    hedge_file = os.path.join(upload_dir, 'input/hedge_input.csv')
    if not(os.path.exists(hedge_file)):
        hedge_file = None  # Hedge file not provided in the input

    return ead_file, ctp_mapping_file, hedge_file

@app.route('/sacva', methods=['POST'])
def sacva():
    """Calculate sacva """
    # clean up existing input and output directories
    create_dir()

    # Save input files to uploads/input folder
    save_files()

    # Read input file paths
    sensi_file, hedge_sensi_file, csr_mapping_file, com_mapping_file, csr_ref_mapping_file, csr_hedge_index_composition_file = read_sacva_filepaths()

    # calculate bacva
    dict_df_data = calculate_sacva(sensi_file, hedge_sensi_file, csr_mapping_file, com_mapping_file, 
                            csr_ref_mapping_file, csr_hedge_index_composition_file)

    # write results to results folder
    result_file = os.path.join(result_dir, 'Summary.xls')
    write_results_sacva(dict_df_data, result_file)

    # zip the results
    ziph = zipfile.ZipFile(os.path.join(
        result_dir, 'results.zip'), 'w', zipfile.ZIP_DEFLATED)
    zipdir(result_dir, ziph)
    ziph.close()

    # send results to the webpage for download
    # it is sending the full folder path as zip. check syntax for this
    # function. Also display a popup saying calculating as it may take some
    # time
    return send_from_directory(result_dir, 'results.zip')

    # Go to the main page
    return redirect('/')

def read_sacva_filepaths():
    upload_dir = app.config['UPLOAD_FOLDER']
    curr_dir = os.path.abspath(os.path.dirname(__file__))
    sensi_file = os.path.join(curr_dir, 'input/sensi_input.csv')
    hedge_sensi_file = os.path.join(curr_dir, 'input/hedge_sensi_input.csv')
    if not(os.path.exists(hedge_sensi_file)):
        hedge_file = None  # Hedge file not provided in the input
    
    csr_hedge_index_composition_file = os.path.join(curr_dir, 'input/index_composition.csv')
    if not(os.path.exists(csr_hedge_index_composition_file)):
        csr_hedge_index_composition_file = None  # Index composition file not provided in the input
    
    csr_mapping_file = os.path.join(curr_dir, 'input/csr_bucket_mapping.csv')
    if not(os.path.exists(csr_mapping_file)):
        csr_mapping_file = None  # csr mapping file not provided in the input
    
    com_mapping_file = os.path.join(curr_dir, 'input/com_bucket_mapping.csv')
    if not(os.path.exists(com_mapping_file)):
        com_mapping_file = None  # com mapping file not provided in the input
    
    csr_ref_mapping_file = os.path.join(curr_dir, 'input/csr_bucket_mapping.csv')
    if not(os.path.exists(csr_ref_mapping_file)):
        csr_ref_mapping_file = None  # csr reference file not provided in the input

    return sensi_file, hedge_sensi_file, csr_mapping_file, com_mapping_file, csr_ref_mapping_file, csr_hedge_index_composition_file

if __name__ == '__main__':
    app.run(debug=True)
