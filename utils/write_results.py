import os
import numpy as np
import pandas as pd
import shutil
import xlwt
import locale

def write_results_to_xls(wb, sheet_name, dataframe, column_styles):
    # Add formats
    styles = {  
        'style_title':xlwt.easyxf('font: name Calibri,bold on; align: horiz left;borders: top thin, bottom thin, right thin, left thin'),
        'style_input':xlwt.easyxf('pattern: pattern solid, fore_colour pale_blue;font: name Calibri; align: horiz left;borders: top thin, bottom thin, right thin, left thin',num_format_str='#,##0.0000'),
        'style_output':xlwt.easyxf('pattern: pattern solid, fore_colour light_yellow;font: name Calibri; align: horiz left;borders: top thin, bottom thin, right thin, left thin',num_format_str='#,##0'),
        'style_output_4digits':xlwt.easyxf('pattern: pattern solid, fore_colour light_yellow;font: name Calibri; align: horiz left;borders: top thin, bottom thin, right thin, left thin',num_format_str='#,##0.0000'),
        'style_final':xlwt.easyxf('pattern: pattern solid, fore_colour light_green;'
                              'font: name Calibri,bold on,height 280;'
                              'borders: top thin, bottom thin, right thin, left thin',num_format_str='#,##0.00')
        }
    # Add sheet
    ws = wb.add_sheet(sheet_name)

    # Get header labels from the dataframe columns
    header = dataframe.columns.values.tolist()

    # Add headers to the sheet
    for col_num, value in enumerate(header):
        ws.write(0, col_num, value, styles['style_title'])

    # Add data to the sheet
    default_style = 'style_output_4digits' # use this if no style has been provided for the column
    [nb_rows, nb_cols] = dataframe.shape
    for row in xrange(nb_rows):
        for col in xrange(nb_cols):
            column_name = header[col]
            if column_name in column_styles.keys():
                column_style = column_styles[column_name]
            else:
                column_style = default_style
            ws.write(row+1, col, dataframe.iloc[row][col], styles[column_style])
