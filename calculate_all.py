# -*- coding: utf-8 -*-
"""
Created on 2018 May 12 18:58:34 2018

@author: zjamal
"""

#This script will calculate all the 3 charges in 1 go for a set of sample trades and sample portfolios
import os
import xlrd
import xlwt
import numpy as np
import datetime
from SM_CVA import Main as smcva
from BA_CVA import bacva_calculation as bacva
from SA_CVA import Main as sacva
from SA_CVA_FUTURE_OPTION1 import Main as sacva_future_option1
from SA_CVA_FUTURE_OPTION2 import Main as sacva_future_option2

def logger(s):
	time = datetime.datetime.now()
	print(str(time) + '--' + s)

def read_results_smcva(file):
	wb = xlrd.open_workbook(file)
	sheets = wb.sheets()
	summary_sheet = sheets[3] 
	row, col = 1, 2
	result = summary_sheet.cell(row, col).value
	#print(result)
	return float(result)

def read_results_bacva(file):
	wb = xlrd.open_workbook(file)
	sheets = wb.sheets()
	summary_sheet = sheets[3]
	row, col = 1, 2
	result = summary_sheet.cell(row, col).value
	#print(result)
	return float(result)

def read_results_sacva(file):
	wb = xlrd.open_workbook(file)
	sheets = wb.sheets()
	summary_sheet = sheets[0]
	row, col = 1, 3
	result = summary_sheet.cell(row, col).value
	#print(result)
	return float(result)

def write_results(matrix, file):
	wb = xlwt.Workbook()
	ws = wb.add_sheet('Summary')
	for row in range(matrix.shape[0]):
		for col in range(matrix.shape[1]):
			ws.write(row, col, matrix[row][col])

	wb.save(file)


def calculate_cva_charge_sample_trades():
	curr_dir = os.path.abspath(os.path.dirname(__file__))
	input_dir = os.path.join(curr_dir, 'cva_sample_trades_input') 

	nb_trades = 4
	method_types = ['smcva', 'bacva', 'sacva', 'sacva_future']
	agreement_types = ['nocsa', 'csa']
	ctp_types = ['ig', 'hy']
	cva_results = []
	for i in range(nb_trades):
		for ctp_type in ctp_types:
			for method_type in method_types:
				for agreement_type in agreement_types:
					if method_type == 'smcva':
						filename = 'ead_input'
						input_file = os.path.join(input_dir, '_'.join(('trade'+str(i+1), filename, method_type, agreement_type, ctp_type))+'.csv')
						smcva.Calculate_SMCVA(input_file)
						output_file = os.path.join(curr_dir, 'SM_CVA/results/Summary.xls')
						cva_results.append(read_results_smcva(output_file))
					elif method_type == 'bacva':
						filename = 'ead_input'
						input_file = os.path.join(input_dir, '_'.join(('trade'+str(i+1), filename, method_type, agreement_type, ctp_type))+'.csv')
						bacva.Calculate_BACVA(input_file)
						output_file = os.path.join(curr_dir, 'BA_CVA/results/Summary.xls')
						cva_results.append(read_results_bacva(output_file))
					elif method_type == 'sacva':
						filename = 'sensi_input'
						input_file = os.path.join(input_dir, '_'.join(('trade'+str(i+1), filename, method_type, agreement_type, ctp_type))+'.csv')
						output_file = os.path.join(curr_dir, 'SA_CVA/results/Summary.xls')
						sacva.Calculate_SACVA(input_file)
						cva_results.append(read_results_sacva(output_file))
					else:
						filename = 'sensi_input'
						method_type_local = 'sacva' #filename for future option is the same as normal sacva
						input_file = os.path.join(input_dir, '_'.join(('trade'+str(i+1), filename, method_type_local, agreement_type, ctp_type))+'.csv')
						output_file1 = os.path.join(curr_dir, 'SA_CVA_FUTURE_OPTION1/results/Summary.xls')
						output_file2 = os.path.join(curr_dir, 'SA_CVA_FUTURE_OPTION2/results/Summary.xls')
						sacva_future_option1.Calculate_SACVA(input_file)
						future_sacva_option1 = read_results_sacva(output_file1)
						sacva_future_option2.Calculate_SACVA(input_file)
						future_sacva_option2 = read_results_sacva(output_file2)
						future_sacva_charge = (future_sacva_option1 + future_sacva_option2)/2.0
						cva_results.append(future_sacva_charge)
					
	results = np.asarray(cva_results, dtype=np.float64)
	x = nb_trades
	y =len(method_types)*len(agreement_types)*len(ctp_types)
	results_matrix = np.reshape(results, (x,y))
	#print(results_matrix)
	notional = 1000000.0
	results_matrix_percentage = np.divide(results_matrix, notional)
	#print(results_matrix_percentage)
	return results_matrix_percentage

def calculate_cva_charge_sample_portfolios():
	curr_dir = os.path.abspath(os.path.dirname(__file__))
	input_dir = os.path.join(curr_dir, 'cva_sample_portfolios_input') 

	# method_types = ['smcva', 'bacva', 'sacva']
	method_types = ['bacva']
	cva_results, cva_results_hedge_ratio_1, cva_results_hedge_ratio_half = [], [], []
	cva_results_hedge_ratio_1_mr_only = []
	cva_results_hedge_ratio_half_mr_only = []
	
	for method_type in method_types:
		if method_type == 'smcva':
			filename = 'ead_input'
			input_file = os.path.join(input_dir, '_'.join((filename, method_type))+'.csv') 
			output_file = os.path.join(curr_dir, 'SM_CVA/results/Summary.xls')
			smcva.Calculate_SMCVA(input_file)
			smcva_base_results = read_results_smcva(output_file)
			cva_results.append(smcva_base_results)
			smcva.Calculate_SMCVA_With_Hedge_Ratio(input_file, 1.0, 0.0, True)
			cva_results_hedge_ratio_1.append(read_results_smcva(output_file))
			smcva.Calculate_SMCVA_With_Hedge_Ratio(input_file, 0.5, 0.0, True)
			cva_results_hedge_ratio_half.append(read_results_smcva(output_file))
			cva_results_hedge_ratio_1_mr_only.append(smcva_base_results)
			cva_results_hedge_ratio_half_mr_only.append(smcva_base_results)
		elif method_type == 'bacva':
			filename = 'ead_input'
			input_file = os.path.join(input_dir, '_'.join((filename, method_type))+'.csv')
			ctp_mapping_file = os.path.join(input_dir, 'cpty_mapping_file_bacva.csv') 
			# output_file = os.path.join(curr_dir, 'BA_CVA/results/Summary.xls')
			df_bacva_results = bacva.calculate_bacva(input_file, ctp_mapping_file, None)
			bacva_base_results = df_bacva_results['df_cva_results'].loc[:,'K_full'].values[0]
			cva_results.append(bacva_base_results)
			
			# hedge ratio 1
			df_bacva_results = bacva.calculate_bacva(input_file, ctp_mapping_file, None, use_hedge_ratio=True, single_name_hedge_ratio=1.0, credit_index_hedge_ratio=0.0)
			bacva_hedge_results = df_bacva_results['df_cva_results'].loc[:,'K_full'].values[0]
			cva_results_hedge_ratio_1.append(bacva_hedge_results)
			
			# hedge ratio 0.5
			df_bacva_results = bacva.calculate_bacva(input_file, ctp_mapping_file, None, use_hedge_ratio=True, single_name_hedge_ratio=0.5, credit_index_hedge_ratio=0.0)
			bacva_hedge_results = df_bacva_results['df_cva_results'].loc[:,'K_full'].values[0]
			cva_results_hedge_ratio_half.append(bacva_hedge_results)
			
			# MR hedges only
			cva_results_hedge_ratio_1_mr_only.append(bacva_base_results) #no change in base results for market hedges
			cva_results_hedge_ratio_half_mr_only.append(bacva_base_results)	#no change in base results for market hedges			
		else:
			filename = 'sensi_input'
			input_file = os.path.join(input_dir, '_'.join((filename, method_type))+'.csv') 
			output_file = os.path.join(curr_dir, 'SA_CVA/results/Summary.xls')
			
			logger('Calculating SACVA')
			sacva.Calculate_SACVA(input_file)
			cva_results.append(read_results_sacva(output_file))

			logger('Calculating SACVA - Hedge Ratio 1')
			sacva.Calculate_SACVA_Hedge_Ratio_1(input_file)
			cva_results_hedge_ratio_1.append(read_results_sacva(output_file))

			logger('Calculating SACVA - Hedge Ratio 0.5')
			sacva.Calculate_SACVA_Hedge_Ratio_Half(input_file)
			cva_results_hedge_ratio_half.append(read_results_sacva(output_file))

			logger('Calculating SACVA - MR Hedge Ratio 1')		
			sacva.Calculate_SACVA_Hedge_Ratio_1(input_file, False) #no credit included
			cva_results_hedge_ratio_1_mr_only.append(read_results_sacva(output_file))

			logger('Calculating SACVA - MR Hedge Ratio 0.5')
			sacva.Calculate_SACVA_Hedge_Ratio_Half(input_file, False) #no credit included
			cva_results_hedge_ratio_half_mr_only.append(read_results_sacva(output_file))

	results_list = [cva_results, cva_results_hedge_ratio_1, cva_results_hedge_ratio_half, cva_results_hedge_ratio_1_mr_only, cva_results_hedge_ratio_half_mr_only] 
	results = np.asarray(results_list, dtype=np.float64)
	print(results)
	#x = nb_trades
	#y =len(method_types)*len(agreement_types)*len(ctp_types)
	#results_matrix = np.reshape(results, (4,12))
	#normalisation_factor = 1.0 #normalisation by a factor - SMCVA results for example

	return results

def calculate_future_sacva_charge_option1_ptfs():
	curr_dir = os.path.abspath(os.path.dirname(__file__))
	input_dir = os.path.join(curr_dir, 'cva_sample_portfolios_input')
	method_type = 'sacva' 
	cva_results, cva_results_hedge_ratio_1, cva_results_hedge_ratio_half = [], [], []
	cva_results_hedge_ratio_1_mr_only = []
	cva_results_hedge_ratio_half_mr_only = []
	filename = 'sensi_input'
	input_file = os.path.join(input_dir, '_'.join((filename, method_type))+'.csv') 
	output_file = os.path.join(curr_dir, 'SA_CVA_FUTURE_OPTION1/results/Summary.xls')
	sacva_future_option1.Calculate_SACVA(input_file)
	cva_results.append(read_results_sacva(output_file))
	sacva_future_option1.Calculate_SACVA_Hedge_Ratio_1(input_file)
	cva_results_hedge_ratio_1.append(read_results_sacva(output_file))
	sacva_future_option1.Calculate_SACVA_Hedge_Ratio_Half(input_file)
	cva_results_hedge_ratio_half.append(read_results_sacva(output_file))
	sacva_future_option1.Calculate_SACVA_Hedge_Ratio_1(input_file, False) #no credit included
	cva_results_hedge_ratio_1_mr_only.append(read_results_sacva(output_file))
	sacva_future_option1.Calculate_SACVA_Hedge_Ratio_Half(input_file, False) #no credit included
	cva_results_hedge_ratio_half_mr_only.append(read_results_sacva(output_file))

	results_list = [cva_results, cva_results_hedge_ratio_1, cva_results_hedge_ratio_half, cva_results_hedge_ratio_1_mr_only, cva_results_hedge_ratio_half_mr_only]
	results = np.asarray(results_list, dtype=np.float64)
	#print(results)
	return results

def calculate_future_sacva_charge_option2_ptfs():
	curr_dir = os.path.abspath(os.path.dirname(__file__))
	input_dir = os.path.join(curr_dir, 'cva_sample_portfolios_input')
	method_type = 'sacva' 
	cva_results, cva_results_hedge_ratio_1, cva_results_hedge_ratio_half = [], [], []
	cva_results_hedge_ratio_1_mr_only = []
	cva_results_hedge_ratio_half_mr_only = []
	filename = 'sensi_input'
	input_file = os.path.join(input_dir, '_'.join((filename, method_type))+'.csv') 
	output_file = os.path.join(curr_dir, 'SA_CVA_FUTURE_OPTION2/results/Summary.xls')
	sacva_future_option2.Calculate_SACVA(input_file)
	cva_results.append(read_results_sacva(output_file))
	sacva_future_option2.Calculate_SACVA_Hedge_Ratio_1(input_file)
	cva_results_hedge_ratio_1.append(read_results_sacva(output_file))
	sacva_future_option2.Calculate_SACVA_Hedge_Ratio_Half(input_file)
	cva_results_hedge_ratio_half.append(read_results_sacva(output_file))
	sacva_future_option2.Calculate_SACVA_Hedge_Ratio_1(input_file, False) #no credit included
	cva_results_hedge_ratio_1_mr_only.append(read_results_sacva(output_file))
	sacva_future_option2.Calculate_SACVA_Hedge_Ratio_Half(input_file, False) #no credit included
	cva_results_hedge_ratio_half_mr_only.append(read_results_sacva(output_file))

	results_list = [cva_results, cva_results_hedge_ratio_1, cva_results_hedge_ratio_half, cva_results_hedge_ratio_1_mr_only, cva_results_hedge_ratio_half_mr_only]
	results = np.asarray(results_list, dtype=np.float64)
	#print(results)
	return results


if __name__ == '__main__':
	curr_dir = os.path.abspath(os.path.dirname(__file__))
	results_file1 = os.path.join(curr_dir, 'summary_sample_trades.xls')
	results_file2 = os.path.join(curr_dir, 'summary_sample_portfolios.xls')
	results_file3 = os.path.join(curr_dir, 'summary_future_sacva_sample_portfolios.xls')

	logger('Starting calculation')
	#write_results(calculate_cva_charge_sample_trades(), results_file1)
	write_results(calculate_cva_charge_sample_portfolios(), results_file2)
	# future_sacva_option1 = calculate_future_sacva_charge_option1_ptfs()
	# future_sacva_option2 = calculate_future_sacva_charge_option2_ptfs()
	# future_sacva_sample_portfolios = (future_sacva_option1 + future_sacva_option2)/2.0
	# write_results(future_sacva_sample_portfolios, results_file3)
	logger('Finished calculation')



