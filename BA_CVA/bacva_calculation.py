# coding=utf-8
import os
import math
import shutil
import numpy as np
import pandas as pd
import xlwt
from read_input import *

# bacva parameters
rho = 0.5
alpha = 1.4
beta = 0.25
Hedge_RW = 0.0245
RW = [[0.005, 0.01, 0.05, 0.03, 0.03, 0.02, 0.015, 0.05],  # RW for IG
      [0.03, 0.04, 0.12, 0.07, 0.085, 0.055, 0.05, 0.12]]  # RW for HY_NR


def calculate_bacva(ead_file, ctp_mapping_file, hedge_file=None,
                    use_hedge_ratio=False,
                    single_name_hedge_ratio=0.0, credit_index_hedge_ratio=0.0):
    """Read input files, calculate bacva capital charge
    Return input and output results as dictionary of dataframes
    """
    # Read input files and convert them to dataframe objects
    df_ctp_details, df_hedge_details, df_ctp_mapping = read_all_input(
        ead_file, hedge_file, ctp_mapping_file)

    # Enrich inputs with additional columns (eg - discount factor)
    df_ctp_details = enrich_ctp_input(df_ctp_details)
    if df_hedge_details is not None:
        df_hedge_details = enrich_hedge_input(df_hedge_details)

    # calculate bacva
    [df_cva_by_counterparty, df_cva_results] = run_bacva_calculation(
        df_ctp_details, df_hedge_details, df_ctp_mapping, use_hedge_ratio, single_name_hedge_ratio, credit_index_hedge_ratio)

    # create a dictionary of all the input and output dataframes to return
    dict_df = {
        'df_ctp_details': df_ctp_details,
        'df_hedge_details': df_hedge_details,
        'df_cva_by_counterparty': df_cva_by_counterparty,
        'df_cva_results': df_cva_results
    }

    return dict_df


def run_bacva_calculation(df_ctp_details, df_hedge_details, df_ctp_mapping,
                          use_hedge_ratio=False,
                          single_name_hedge_ratio=0.0, credit_index_hedge_ratio=0.0):
    """Calculate bacva capital charge
    """
    nb_inputs = len(df_ctp_details)
    if nb_inputs > 0:
        df_ctp_details['product_ead_m_df'] = df_ctp_details['ead'] * df_ctp_details['maturity'] * df_ctp_details['df']

        # sum of ead, M, DF over all netting sets
        df_cva_by_counterparty = df_ctp_details.groupby(
            ['counterparty']).agg({'product_ead_m_df': np.sum})

        # groupby function returns the counterparty key as index labels. We
        # will reset it as a counterparty column
        df_cva_by_counterparty.reset_index(level=0, inplace=True)

        # add column risk weight
        df_cva_by_counterparty['riskweight'] = df_cva_by_counterparty['counterparty'].apply(get_counterparty_riskweight, args=(df_ctp_mapping,))

        # add column scva : scva = 1/α*RW*∑EAD*M(NS)*DF
        df_cva_by_counterparty['scva'] = 1 / alpha * df_cva_by_counterparty['riskweight'] * \
            df_cva_by_counterparty['product_ead_m_df']

        # calculate sum of cva and sum of cva sqaured across all counterparties
        scva_sum = df_cva_by_counterparty['scva'].sum()
        scva_square_sum = (df_cva_by_counterparty['scva']**2).sum()

        # calculate K(reduced) : K(reduced) = √(〖(𝜌.∑𝑆𝐶𝑉𝐴)〗^2+(1−𝜌^2
        # ).∑〖𝑆𝐶𝑉𝐴〗^2 )
        K_reduced = math.sqrt((rho * scva_sum)**2 +
                              (1 - rho**2) * scva_square_sum)

        # check if hedges need to be considered
        is_hedge_present = use_hedge_ratio or df_hedge_details is not None
        if not (is_hedge_present):
            ih_sum = 0.0
            hma_sum = 0.0
            K_systemic = scva_sum
            K_specific = scva_square_sum
            K_hedged = K_reduced
        else:
            if use_hedge_ratio:
                r_hc = single_name_hedge_ratio
                df_snh_by_counterparty = df_cva_by_counterparty[['counterparty', 'scva']]
                # rename counterparty column to hedge_ctp column
                df_snh_by_counterparty.columns = ['hedge_ctp', 'scva']

                df_snh_by_counterparty['snh'] = r_hc**2 * df_snh_by_counterparty['scva']
                if r_hc == 0:
                    hma_ratio = 0
                else:
                    # division by r_hc^2 compensates for the fact that r_hc is
                    # included in SNH definition
                    hma_ratio = (1 - r_hc**2) / r_hc**2

                df_snh_by_counterparty['hma_ratio'] = hma_ratio
                df_snh_by_counterparty['hma'] = hma_ratio * (df_snh_by_counterparty['snh']**2)
            else:
                # store the single name hedge in separate dataframe
                df_snh_details = df_hedge_details[df_hedge_details['hedge_type'] == 'single_name'].copy()

                # add column risk weight for single name hedges by using the counterparty risk weight logic
                # index hedge risk weights are provided in the input file
                df_snh_details['riskweight'] = df_snh_details['hedge_ctp'].apply(
                    get_counterparty_riskweight, args=(df_ctp_mapping,))

                df_snh_details['snh'] = df_snh_details['r_hc'] * df_snh_details['riskweight'] * \
                    df_snh_details['maturity'] * \
                    df_snh_details['notional'] * df_snh_details['df']

                # sum all hedges by counterparty
                df_snh_by_counterparty = df_snh_details.groupby(
                    ['hedge_ctp']).agg({'snh': np.sum, 'r_hc': np.mean})
                # groupby function returns the counterparty key as index labels. 
                # We will reset it as a counterparty column
                df_snh_by_counterparty.reset_index(level=0, inplace=True)

                df_snh_by_counterparty['hma_ratio'] = (1 - df_snh_by_counterparty['r_hc']**2) / df_snh_by_counterparty['r_hc']
                df_snh_by_counterparty['hma'] = df_snh_by_counterparty['hma_ratio'] * (df_snh_by_counterparty['snh']**2)

                # store the index hedge in separate dataframe
                df_ih_details = df_hedge_details[
                    df_hedge_details['hedge_type'] == 'index'].copy()
                df_ih_details['ih'] = df_ih_details['riskweight'] * df_ih_details['maturity'] * \
                    df_ih_details['notional'] * df_ih_details['df']

            # merge cva and hedge dataframes to get cva and hedge countribution
            # per counterparty
            df_cva_merged = pd.merge(df_cva_by_counterparty,
                                     df_snh_by_counterparty[['hedge_ctp', 'snh', 'hma']],
                                     left_on='counterparty',
                                     right_on='hedge_ctp',
                                     how='outer').drop('hedge_ctp', 1)  # pandas includes both keys in the merge results. Drop the second one using drop

            # As the merge above will populate NaN for columns which are not
            # present in the other table, we will replace NaN values in the
            # dataframe with 0
            df_cva_merged.fillna(0, inplace=True)

            scva_minus_snh_sum = (df_cva_merged['scva'] - df_cva_merged['snh']).sum()
            scva_minus_snh_square_sum = ((df_cva_merged['scva'] - df_cva_merged['snh'])**2).sum()

            if use_hedge_ratio:
                ih_sum = credit_index_hedge_ratio * (scva_minus_snh_sum)
            else:
                ih_sum = df_ih_details['ih'].sum()

            hma_sum = df_cva_merged['hma'].sum()

            # calculate K(hedged) : K(hedged) = √((𝜌.∑〖(𝑆𝐶𝑉𝐴−𝑆𝑁𝐻)〗−𝐼𝐻)^2 +
            # (1−𝜌^2 ).∑〖(𝑆𝐶𝑉𝐴−𝑆𝑁𝐻)〗^2〗+∑𝐻𝑀𝐴)
            K_systemic = (rho * scva_minus_snh_sum - ih_sum)**2
            K_specific = (1 - rho**2) * (scva_minus_snh_square_sum)
            K_hedged = math.sqrt((K_systemic + K_specific + hma_sum))

        # final cva capital charge
        K_full = beta * K_reduced + (1 - beta) * K_hedged

        df_columns = ['ih_sum', 'K_systemic', 'K_specific',
                      'hma_sum', 'K_reduced', 'K_hedged', 'K_full']
        df_cva_results = pd.DataFrame(
            [[ih_sum, K_systemic, K_specific, hma_sum, K_reduced, K_hedged, K_full]], columns=df_columns)

        if is_hedge_present:
            return df_cva_merged, df_cva_results
        else:
            return df_cva_by_counterparty, df_cva_results


def get_counterparty_riskweight(counterparty, df_ctp_mapping):
    """Return risk weight corresponding to counterparty's regulatory bucket
    """
    riskweight = 0.0
    if len(df_ctp_mapping) == 0:
        return riskweight

    bucket_mapping = df_ctp_mapping.loc[
        df_ctp_mapping['counterparty'] == counterparty, 'bucket']
    if len(bucket_mapping) == 0:
        # print("Counterparty {} is not mapped to a bucket. RW of 0.0 will be assumed".format(counterparty))
        return riskweight
    else:
        # loc returns a series. values casts it to a numpy array. 0 selects the
        # first and only entry
        bucket_value = bucket_mapping.values[0]
        # bucket input is a combination of bucket and quality - eg-01_HY_NR
        mapping_split = bucket_value.split('_')
        bucket_str = mapping_split[0]
        quality_grade = mapping_split[1:]

        if '1a' in bucket_str:
            bucket = 0
        elif '1b' in bucket_str:
            bucket = 1
        else:
            bucket = int(bucket_str)

        if 'IG' in quality_grade:
            riskweight = RW[0][bucket]
        else:
            riskweight = RW[1][bucket]

    return riskweight
