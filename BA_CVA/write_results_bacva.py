# -*- coding: utf-8 -*-
import os
import sys
import xlwt

curr_dir = os.path.abspath(os.path.dirname(__file__))


def write_results_bacva(dict_df_data, result_file):
    """Write bacva input and output results"""

    # retrive input and output results from the dictionary
    df_ctp_details, df_hedge_details = dict_df_data[
        'df_ctp_details'], dict_df_data['df_hedge_details']
    df_cva_by_counterparty, df_cva_results = dict_df_data[
        'df_cva_by_counterparty'], dict_df_data['df_cva_results']

    # add utils folder to sys path which contains the write_results script
    sys.path.append(os.path.abspath(os.path.join(curr_dir, '../utils')))
    from write_results import write_results_to_xls

    # create workbook
    wb = xlwt.Workbook()

    # write EAD inputs
    sheet_name = 'EAD Input'
    column_styles = {
        'counterparty': 'style_input',
        'netting_set': 'style_input',
        'ead': 'style_input',
        'maturity': 'style_input',
        'df': 'style_output_4digits',
        'product_ead_m_df': 'style_output_4digits'
    }
    write_results_to_xls(wb, sheet_name, df_ctp_details, column_styles)

    # write hedge inputs
    if df_hedge_details is not None:
        sheet_name = 'Hedge Input'
        column_styles = {
            'issuer': 'style_input',
            'hedge_ctp': 'style_input',
            'maturity': 'style_input',
            'notional': 'style_input',
            'hedge_type': 'style_input',
            'riskweight': 'style_input',
            'r_hc': 'style_input',
            'df': 'style_output_4digits'
        }
        write_results_to_xls(wb, sheet_name, df_hedge_details, column_styles)

    # write cva results by counterparty
    sheet_name = 'CVA by Counterparty'
    column_styles = {
        'counterparty': 'style_input',
        'product_ead_m_df': 'style_output_4digits',
        'riskweight': 'style_output_4digits',
        'scva': 'style_output_4digits'
    }
    write_results_to_xls(wb, sheet_name, df_cva_by_counterparty, column_styles)

    # write final cva results
    sheet_name = 'Summary'
    column_styles = {
        'ih_sum': 'style_output_4digits',
        'K_systemic': 'style_output_4digits',
        'K_specific': 'style_output_4digits',
        'hma_sum': 'style_output_4digits',
        'K_reduced': 'style_output_4digits',
        'K_hedged': 'style_output_4digits',
        'K_full': 'style_output_4digits',
    }
    write_results_to_xls(wb, sheet_name, df_cva_results, column_styles)

    if result_file:
        wb.save(result_file)
