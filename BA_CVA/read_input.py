import os
import numpy as np
import pandas as pd
import shutil
import xlwt
import locale


def process_tenor(Tenor):
    try:
        result=Tenor.strip().lower()
        result=result.replace('yr','')
        result=result.replace('y','')
        if len(result)>0:
            if 'm' in result:
                result=result.replace('m','')
                result=float(result)/12.0
            else:
                result=float(result)
        else:
            result=0.0
    except:
        result=0.0
    return result
  
  
def read_ead_input(fileName):
    """Read counterparty ead input csv file. 
    Return a dataframe with columns 'counterparty', 'netting_set', 'ead', 'maturity'
    """

    with open(fileName) as infile:
        next(infile) #ignore the first line
        ead_data=[]
        for line in infile:
            line=line.replace('"','')
            contents=line.strip().split(';')            
            ead_info=[]
            try:
                counterparty=contents[0].strip()
                nettingset=contents[1].strip()
                EAD=contents[2].strip()
                maturity=contents[3].strip()
                ead_info.append(counterparty)
                ead_info.append(nettingset)
                ead_info.append(EAD)
                ead_info.append(process_tenor(maturity))
                ead_data.append(ead_info)               
            except ValueError:
                print "Please check %s."%line

        if len(ead_data)>0:
            ead_data=np.array(ead_data)
   
        df_columns = ['counterparty', 'netting_set', 'ead', 'maturity']
        df_ctp_details = pd.DataFrame(ead_data, columns=df_columns)
        df_ctp_details[['ead', 'maturity']] = df_ctp_details[['ead', 'maturity']].apply(pd.to_numeric)
        return df_ctp_details

  
def read_hedge_input(fileName):
    """Read hedge input csv file. 
    Return a dataframe with columns 'counterparty', 'netting_set', 'ead', 'maturity'
    """
    with open(fileName) as infile:
        next(infile) #ignore the first line
        hedge_data=[]
        for line in infile:
            contents=line.strip().split(';')         
            hedge_info=[]
            try:
                issuer=contents[0].strip()
                hedge_counterparty = contents[1].strip()
                maturity=contents[2].strip()
                notional=contents[3].strip()
                hedge_type = contents[4].strip().lower()
                riskweight = contents[5].strip()
                if riskweight == '':
                    riskweight = 0.0
                r_hc = contents[6].strip()
                if r_hc == '':
                    r_hc = 0.0  
                hedge_info.append(issuer)
                hedge_info.append(hedge_counterparty)
                hedge_info.append(process_tenor(maturity))
                hedge_info.append(notional)
                hedge_info.append(hedge_type)
                hedge_info.append(riskweight)
                hedge_info.append(r_hc)
                hedge_data.append(hedge_info)               
            except ValueError:
                print "Please check %s."%line
        
        if len(hedge_data)>0:
            np_hedge_data=np.array(hedge_data)
       
        df_columns = ['issuer', 'hedge_ctp', 'maturity', 'notional', 'hedge_type', 'riskweight', 'r_hc']
        df_hedge_details = pd.DataFrame(np_hedge_data, columns=df_columns)
        df_hedge_details = df_hedge_details.astype(dtype = {'maturity':'float', 'notional':'int32', 'riskweight':'float', 'r_hc':'float'}) #xlwt can not handle datatype int64, so have to explicityly cast notional to int32
        
        return df_hedge_details

#read mapping csv file and store as npy file     
def read_mapping(fileName):
    ctp_mapping=[]
    with open(fileName) as infile:
        next(infile) #ignore the first line
        for line in infile:
            contents=line.strip().split(';')
            #remove 0 if the bucket number starts with 0
            if len(contents)>1:
                try: 
                    if contents[1].startswith('0'):
                        contents[1]=contents[1][1:]
                    ctp_mapping.append(contents)
                except ValueError:
                    print "Not a float @ line %s"%line
    
    np_ctp_mapping = np.array(ctp_mapping)
    df_columns = ['counterparty', 'bucket']
    df_ctp_mapping = pd.DataFrame(np_ctp_mapping, columns=df_columns)
    return df_ctp_mapping


def read_all_input(ead_file, hedge_file, ctp_mapping_file):
    """Read ead, hedge and counterparty mapping csv files and store them as dataframes
    """
    if os.path.exists(ead_file):  
        df_ctp_details = read_ead_input(ead_file)

    if hedge_file is not None and os.path.exists(hedge_file):
        df_hedge_details = read_hedge_input(hedge_file)
    else:
        df_hedge_details = None
    
    if os.path.exists(ctp_mapping_file):
        df_ctp_mapping = read_mapping(ctp_mapping_file)

    return df_ctp_details, df_hedge_details, df_ctp_mapping

def enrich_ctp_input(df_ctp_details):
    """Enrich counterparty details with additional columns like discount factor
    """
    df_ctp_details['df'] = df_ctp_details['maturity'].apply(lambda t:(1-np.exp(-0.05*t))/(0.05*t))
    return df_ctp_details

def enrich_hedge_input(df_hedge_details):
    """Enrich counterparty details with additional columns like discount factor
    """
    df_hedge_details['df'] = df_hedge_details['maturity'].apply(lambda t:(1-np.exp(-0.05*t))/(0.05*t))
    return df_hedge_details