# -*- coding: utf-8 -*-
"""
Created on Thu Feb 01 18:58:34 2018

@authors: tding, zjamal
"""

import os
import sys
from bacva_calculation import calculate_bacva
from write_results_bacva import write_results_bacva
import pandas as pd

if __name__ == '__main__':
    # Read filenames from folder 'input'
    curr_dir = os.path.abspath(os.path.dirname(__file__))
    # ead_file = os.path.join(curr_dir,'input/ead_input.csv')
    ead_file = os.path.join(curr_dir, 'input/ead_input_sample.csv')
    ctp_mapping_file = os.path.join(curr_dir, 'input/cpty_mapping_file.csv')
    hedge_file = os.path.join(curr_dir, 'input/hedge_input.csv')

    # calculate bacva
    dict_df_data = calculate_bacva(ead_file, ctp_mapping_file, hedge_file)

    # without hedge file
    # dict_df_data = calculate_bacva(ead_file, ctp_mapping_file, None)

    # with hedge ratio
    # dict_df_data = calculate_bacva(ead_file, ctp_mapping_file, None, use_hedge_ratio=True, single_name_hedge_ratio=1.0, credit_index_hedge_ratio=0.0)

    # write results to xls in the 'results' folder
    result_file = os.path.join(curr_dir, 'results/Summary.xls')
    write_results_bacva(dict_df_data, result_file)
