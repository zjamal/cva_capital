import math
import numpy as np

#Geneal inputs
R=0.01
m=1.25

#IR inputs
LiquidCcy=['EUR','USD','GBP','AUD','JPY','SEK','CAD']
IR_delta_RW_others=0.0225
IR_delta_RW_mapping=[[1,0.0159],[2,0.0133],[5,0.0106],[10,0.0106],[30,0.0159]]
Inflation_RW=0.0225
IR_delta_Correlations=[[1,0.91,0.72,0.55,0.31,0.4],
                       [0.91,1,0.87,0.72,0.45,0.4],
                       [0.72,0.87,1,0.91,0.68,0.4],
                       [0.55,0.72,0.91,1,0.83,0.4],
                       [0.31,0.45,0.68,0.83,1,0.4],
                       [0.4,0.4,0.4,0.4,0.4,1]]
IR_Correlations_Others=0.4
IR_CrossCorrelations=0.5
IR_Vega_RW=0.55*math.sqrt(6)




#FX inputs
FX_Delta_RW=0.21
FX_Delta_RW_JPY=0.21
FX_CrossCorrelations=0.6
FX_Vega_RW=0.55*2


#CSR inputs
CSR_Bucket_RW_mapping=[[0.005,0.01,0.0500,0.0300,0.0300,0.0200,0.0150,0.0500],
                       [0.0300,0.0400,0.1200,0.0700,0.0850,0.0550,0.0500,0.1200]]
CSR_SameEntityDiffTenor=0.9
CSR_UnrelatedSameQualitySameTenor=0.5
CSR_UnrelatedSameQualityDiffTenor=0.45
CSR_UnrelatedDiffQualitySameTenor=0.4
CSR_UnrelatedDiffQualityDiffTenor=0.36
CSR_RelatedSameTenor=0.9
CSR_RelatedDiffTenor=0.81
CSR_CrossCorrelation=[[1,0.1,0.2,0.25,0.2,0.15],
                      [0.1,1,0.05,0.15,0.2,0.05],
                      [0.1,0.05,1,0.2,0.25,0.05],
                      [0.25,0.15,0.2,1,0.25,0.05],
                      [0.2,0.2,0.25,0.25,1,0.05],
                      [0.15,0.05,0.05,0.05,0.05,1]]

#CSR_REF inputs
CSR_REF_Bucket_RW_mapping=[0.0050,0.0100,0.0500,0.0300,0.0300,0.0200,0.0150,0.0300,0.0400,0.1200,0.0700,0.0850,0.0550,0.0500,0.1200]
CSR_REF_CrossCorrelation=[[1,0.75,0.1,0.2,0.25,0.2,0.15],
                      [0.75,1,0.05,0.15,0.2,0.15,0.1],
                      [0.1,0.05,1,0.05,0.15,0.2,0.05],
                      [0.2,0.15,0.05,1,0.2,0.25,0.05],
                      [0.25,0.2,0.15,0.2,1,0.25,0.05],
                      [0.2,0.15,0.2,0.25,0.25,1,0.05],
                      [0.15,0.1,0.05,0.05,0.05,0.05,1]]
CSR_REF_Vega_RW=0.55*math.sqrt(12)

#Commodity inputs
COM_Bucket_RW_mapping=[0.30,0.35,0.60,0.80,0.40,0.45,0.20,0.35,0.25,0.35,0.50]
COM_Vega_RW=0.55*math.sqrt(12)
COM_CBucket_Corr_1to10=0.2
COM_CBucket_Corr_11=0.0



def ProcessTenor(Tenor):
    try:
        result=Tenor.strip().lower()
        result=result.replace('yr','')
        result=result.replace('y','')
        if len(result)>0:
            if 'm' in result:
                result=result.replace('m','')
                result=float(result)/12.0
            else:
                result=float(result)
        else:
            result=0.0
    except:
        result=0.0
    return result
    

######## Asset Class Specific Delta Risk Weight ########        
def Calculate_Delta_RW(AssetClass,info):
    result=0.0
    if AssetClass=='IR_Delta':
        result=Cal_IR_Delta_RW(info)
    elif AssetClass=='FX_Delta':
        contents=info.split(',')
        if contents[0]=='JPY':            
           result=FX_Delta_RW_JPY
        else:result=FX_Delta_RW
    elif AssetClass=='CSR_Delta':
        result=Cal_CSR_Delta_RW(info)
    elif AssetClass=='CSR_REF_Delta':
        result=Cal_CSR_REF_Delta_RW(info)
    elif AssetClass=='COM_Delta':
        result=Cal_COM_Delta_RW(info)
    return result

#info:[bucket,type, tenor]
def Cal_IR_Delta_RW(info):
    result=0.0
    contents=info.split(',')
    ccy=contents[0]
    risk_class=contents[1]
    tenor=contents[2]
    tenor=ProcessTenor(tenor)
    if risk_class=='inflation':
        result=Inflation_RW
    elif ccy in LiquidCcy:
        Nb_RWs=len(IR_delta_RW_mapping)
        for i in range(0,Nb_RWs):
            if tenor==IR_delta_RW_mapping[i][0]:
                result=IR_delta_RW_mapping[i][1]
    else:result=IR_delta_RW_others
    return result
    
#info:[bucket, issuer,issuer parent,credit quality]    
def Cal_CSR_Delta_RW(info):
    result=0.0
    contents=info.split(',')
    CSR_bucket=int(contents[0])
    CSR_quality=str(contents[3])
    if isinstance(CSR_bucket,int):
        if 'IG' in CSR_quality:
            result=CSR_Bucket_RW_mapping[0][CSR_bucket-1]
        else: result=CSR_Bucket_RW_mapping[1][CSR_bucket-1]
    else:
        result=0.0
    return result

#info:[bucket]    
def Cal_CSR_REF_Delta_RW(info):
    result=0.0
    contents=info.split(',')
    CSR_REF_bucket=int(contents[0])
    if isinstance(CSR_REF_bucket,int):
        result=CSR_REF_Bucket_RW_mapping[CSR_REF_bucket-1]
    else:
        result=0.0
    return result

#info:[bucket]      
def Cal_COM_Delta_RW(info):
    result=0.0
    contents=info.split(',')
    com_bucket=int(contents[0])
    if isinstance(com_bucket,int):
        result=COM_Bucket_RW_mapping[com_bucket-1]
    else:
        result=0.0
    return result
    
######## Asset Class Specific Vega Risk Weight ########     
def Calculate_Vega_RW(AssetClass):
    RW=0.0
    if AssetClass=='IR_Vega':
        RW=IR_Vega_RW
    elif AssetClass=='FX_Vega':
        RW=FX_Vega_RW
    elif AssetClass=='CSR_REF_Vega':
        RW=CSR_REF_Vega_RW
    elif AssetClass=='COM_Vega':
        RW=COM_Vega_RW
    return RW

######## Asset Class Specific Within Bucket Correlation ########        
def Calculate_WithinBucketCorrelation(AssetClass,info1,info2):
    if info1==info2:
        result=1
    else: 
        if AssetClass=='IR_Delta':
            result=Cal_IR_Delta_WithinBucketCorrelation(info1,info2)
        elif AssetClass=='FX_Delta':
            result=1.0
        elif AssetClass=='CSR_Delta':
            result=Cal_CSR_Delta_WithinBucketCorrelation(info1,info2)
        elif AssetClass=='COM_Delta':
            result=1.0
        elif AssetClass=='CSR_REF_Delta':
            result=1.0
        elif AssetClass=='IR_Vega':
            result=IR_Correlations_Others
        elif AssetClass=='FX_Vega':
            result=1.0
        elif AssetClass=='CSR_REF_Vega':
            result=1.0
        elif AssetClass=='COM_Vega':
            result=1.0
    return result

#info:[bucket,type, tenor] 
def Cal_IR_Delta_WithinBucketCorrelation(info1,info2):
    result=1.0
    bucket1=0
    bucket2=0
    contents1=info1.split(',')
    risk_class1=contents1[1]
    ccy1=contents1[0]
    tenor1=contents1[2]
    tenor1=ProcessTenor(tenor1)
    if risk_class1=="inflation":
        bucket1=5
    elif tenor1==1:
        bucket1=0
    elif tenor1==2:
        bucket1=1
    elif tenor1==5:
        bucket1=2
    elif tenor1==10:
        bucket1=3
    elif tenor1==30:
        bucket1=4 
    else: bucket1=0
    contents2=info2.split(',')
    risk_class2=contents2[1]
    ccy2=contents2[0]
    tenor2=contents2[2]
    tenor2=ProcessTenor(tenor2)  
    if risk_class2=="inflation":
        bucket2=5
    elif tenor2==1:
        bucket2=0
    elif tenor2==2:
        bucket2=1
    elif tenor2==5:
        bucket2=2
    elif tenor2==10:
        bucket2=3
    elif tenor2==30:
        bucket2=4 
    else: bucket2=0
    if ccy1 in LiquidCcy and ccy2 in LiquidCcy:
        result=IR_delta_Correlations[bucket1][bucket2]
    else:
        result=IR_Correlations_Others
    return result

#info:[bucket, issuer,issuer parent,credit quality]  
def Cal_CSR_Delta_WithinBucketCorrelation(info1,info2):
    result=1.0
    contents1=info1.split(',')
    issuer1=contents1[1]
    if contents1[2]=='': issuerparent1=contents1[1]
    else:issuerparent1=contents[2]
    quality1=contents1[3]
    if 'IG' in quality1:
        quality1=1
    else:quality1=2
    tenor1=contents1[4]
    tenor1=ProcessTenor(tenor1)
    contents2=info2.split(',')
    issuer2=contents2[1]
    if contents2[2]=='':issuerparent2=contents2[1]
    else: issuerparent2=contents2[2]
    quality2=contents2[3]
    if 'IG' in quality2:
        quality2=1
    else:quality2=2
    tenor2=contents2[4]
    tenor2=ProcessTenor(tenor2)
    if issuer1==issuer2 and tenor1==tenor2:
        result=1.0
    elif issuer1==issuer2:
        result=CSR_SameEntityDiffTenor
    elif issuerparent1<>issuerparent2 and quality1==quality2 and tenor1==tenor2:
        result=CSR_UnrelatedSameQualitySameTenor
    elif issuerparent1<>issuerparent2 and quality1==quality2 and tenor1<>tenor2:
        result=CSR_UnrelatedSameQualityDiffTenor
    elif issuerparent1<>issuerparent2 and quality1<>quality2 and tenor1==tenor2:
        result=CSR_UnrelatedDiffQualitySameTenor
    elif issuerparent1<>issuerparent2 and quality1 <> quality2 and tenor1 <>tenor2:
        result=CSR_UnrelatedDiffQualityDiffTenor
    elif issuerparent1==issuerparent2 and tenor1==tenor2: 
        result=CSR_RelatedSameTenor
    elif issuerparent1==issuerparent2 and tenor1 <> tenor2:
        result=CSR_RelatedDiffTenor
    return result
    
######## Asset Class Specific Cross Bucket Correlation ########        
def Calculate_CrossBucketCorrelation(AssetClass,bucket1,bucket2):
    if bucket1==bucket2:
        result=1
    else:
        if AssetClass=='IR_Delta':
            result=IR_CrossCorrelations
        elif AssetClass=='FX_Delta':
            result=FX_CrossCorrelations
        elif AssetClass=='CSR_Delta':
            result=Cal_CSR_Delta_CrossBucketCorrelation(bucket1,bucket2)
        elif AssetClass=='COM_Delta':
            result=Cal_COM_Delta_CrossBucketCorrelation(bucket1,bucket2)
        elif AssetClass=='CSR_REF_Delta':
            result=Cal_CSR_REF_Delta_CrossBucketCorrelation(bucket1,bucket2)
        elif AssetClass=='IR_Vega':
            result=IR_CrossCorrelations
        elif AssetClass=='FX_Vega':
             result=FX_CrossCorrelations
        elif AssetClass=='CSR_REF_Vega':
            result=Cal_CSR_REF_Delta_CrossBucketCorrelation(bucket1,bucket2)
        elif AssetClass=='COM_Vega':
            result=Cal_COM_Delta_CrossBucketCorrelation(bucket1,bucket2)
    return result
    

def Cal_CSR_Delta_CrossBucketCorrelation(bucket1,bucket2):
    bucket1=int(bucket1)
    bucket2=int(bucket2)
    if bucket1==7.0 or bucket2==7.0:
        result=0.0
        return result
    else:
        result=CSR_CrossCorrelation[bucket1-1][bucket2-1]
    return result

def Cal_CSR_REF_Delta_CrossBucketCorrelation(bucket1,bucket2):
    if bucket1==15.0 or bucket2==15.0:
        result=0.0
    elif bucket1>7.0 and bucket2<=7.0:
        result=CSR_REF_CrossCorrelation[bucket1-1][bucket2-1]/2
    elif bucket1<=7.0 and bucket2>7.0:
        result=CSR_REF_CrossCorrelation[bucket1-1][bucket2-1]/2
    else: result=CSR_REF_CrossCorrelation[bucket1-1][bucket2-1]
    return result

def Cal_COM_Delta_CrossBucketCorrelation(bucket1,bucket2):
    if bucket1==bucket2:
        result=1.0
    elif bucket1==11 or bucket2==11:
        result=COM_CBucket_Corr_11
    else :
        result=COM_CBucket_Corr_1to10
    return result
    