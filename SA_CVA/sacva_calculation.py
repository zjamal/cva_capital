# coding=utf-8
import os
import shutil
import numpy as np
import pandas as pd
from read_input import *
from sacva_delta import calculate_delta
from sacva_vega import calculate_vega


def calculate_sacva(sensi_file, hedge_sensi_file=None, 
                    csr_mapping_file=None, com_mapping_file=None, csr_ref_mapping_file=None,
                    csr_hedge_index_composition_file=None, 
                    use_hedge_ratio=False, hedge_ratio=0.0):
    """Read input files, calculate sacva capital charge
    Return input and output results as dictionary of dataframes
    """
    # Read input files and convert them to dataframe objects
    # df_ctp_details, df_hedge_details, df_ctp_mapping = read_all_input(
    #     ead_file, hedge_file, ctp_mapping_file)
    curr_dir = os.path.abspath(os.path.dirname(__file__))
    np_dir = os.path.join(curr_dir,'numpy/')

    # read mapping files
    if csr_mapping_file is not None and os.path.exists(csr_mapping_file):
        csr_mapping = read_mapping(csr_mapping_file)
    else:
        csr_mapping = None

    if com_mapping_file is not None and os.path.exists(com_mapping_file):
        com_mapping = read_mapping(com_mapping_file)
    else:
        com_mapping = None

    if csr_ref_mapping_file is not None and os.path.exists(csr_ref_mapping_file):
        csr_ref_mapping = read_mapping(csr_ref_mapping_file)
    else:
        csr_ref_mapping = None
                    
    if csr_hedge_index_composition_file is not None and os.path.exists(csr_hedge_index_composition_file):
        csr_hedge_index_composition = read_mapping(csr_hedge_index_composition_file)
    else:
        csr_hedge_index_composition = None

    # read sensi input file
    sensi_type = read_sensi_input(np_dir, sensi_file, hedge_sensi_file, csr_mapping, com_mapping, csr_ref_mapping,
                               csr_hedge_index_composition, use_hedge_ratio)
    enable_log = True # temp - to be removed
    summary_inputs = []
    for info in sensi_type:
        if 'Delta' in info:
            Riskcharge = calculate_delta(np_dir, info, enable_log, use_hedge_ratio, hedge_ratio)
        else:
            Riskcharge = calculate_vega(np_dir, info, enable_log, use_hedge_ratio, hedge_ratio)
        output = info + '|' + str(round(Riskcharge, 0))
        summary_inputs.append(output)

    # calculate bacva
    # [df_cva_by_counterparty, df_cva_results] = run_bacva_calculation(
        # df_ctp_details, df_hedge_details, df_ctp_mapping, use_hedge_ratio, single_name_hedge_ratio, credit_index_hedge_ratio)

    # create a dictionary of all the input and output dataframes to return
    # dict_df = {
    #     'df_ctp_details': df_ctp_details,
    #     'df_hedge_details': df_hedge_details,
    #     'df_cva_by_counterparty': df_cva_by_counterparty,
    #     'df_cva_results': df_cva_results
    # }
    dict_df = summary_inputs
    return dict_df
