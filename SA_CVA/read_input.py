import numpy as np
import shutil
import xlwt
import locale
import os


def ProcessFloatOutput(input_str):
    result = ''
    contents = input_str.split('|')
    locale.setlocale(locale.LC_NUMERIC, 'English')
    for content in contents:
        if len(content) > 0:
            if len(result) == 0:
                result = locale.format('%.2f', float(content), True)
            else:
                result = result + '|' + locale.format('%.2f', float(content), True)
    return result


def RiskBucketMapping(issuer, Bucket_mapping):
    result = 'Not mapped'
    if len(Bucket_mapping) == 0:
        return result
    mapping = Bucket_mapping[np.where(Bucket_mapping[:, 0] == issuer)]
    if len(mapping) > 0:
        result = mapping[:, 1][0]
    return result


def IndexMapping(index, index_composition):
    issuer_weight = np.array([])
    if len(index_composition) == 0:
        return issuer_weight
    issuer_weight = index_composition[np.where(index_composition[:, 0] == index[:9])]
    return issuer_weight

# read sensi and mapping csv files and save as np files


def read_sensi_input(dir, filename, sensifileName, CSR_Mapping, COM_Mapping, CSR_REF_Mapping, CSR_Hedge_Index_Composition, Use_Hedge_Ratio):
    with open(filename) as infile:
        IR_delta_data, FX_delta_data, CSR_delta_data, CSR_REF_delta_data, Commodity_delta_data = [], [], [], [], []
        IR_vega_data, FX_vega_data, CSR_REF_vega_data, Commodity_vega_data = [], [], [], []
        risk_class_list = []
        next(infile)  # ignore the first line
        for line in infile:
            line = line.replace(',,', '')
            line = line.replace(',', '')
            contents = line.strip().split(';')
            try:
                risk_class = contents[0].strip()
                bucket = contents[1].strip()
                issuer_parent = contents[3].strip()
                pillar = contents[4].strip()
                sensi = contents[5].strip()
                if float(sensi) <> 0.0:
                    if risk_class == 'IR_Delta' or risk_class == "IR_Delta_Inflation":
                        ir_info = []
                        if risk_class == 'IR_Delta_Inflation':
                            risk_type = 'inflation'
                        else:
                            risk_type = ' '
                        risk_class = 'IR_Delta'
                        label = bucket + ',' + risk_type + ',' + pillar + ',' + 'CVA'
                        ir_info.append(label)
                        ir_info.append(sensi)
                        IR_delta_data.append(ir_info)
                    elif risk_class == 'FX_Delta':
                        fx_info = []
                        label = bucket + ',' + 'CVA'
                        fx_info.append(label)
                        fx_info.append(sensi)
                        FX_delta_data.append(fx_info)
                    elif risk_class == 'CSR_Delta':
                        CSR_info = []
                        CSR_bucket_full = RiskBucketMapping(bucket, CSR_Mapping)
                        if CSR_bucket_full <> 'Not mapped':
                            contents = CSR_bucket_full.split('_')
                            if '1a' in contents[0]:
                                CSR_bucket = 0
                            elif '1b' in contents[0]:
                                CSR_bucket = 1
                            else:
                                CSR_bucket = int(contents[0])
                            quality_grade = contents[1]
                        else:
                            CSR_bucket = 7
                            quality_grade = 'HY'
                        label = str(CSR_bucket) + ',' + bucket + ',' + issuer_parent + ',' + quality_grade + ',' + pillar + ',' + 'CVA'
                        CSR_info.append(label)
                        CSR_info.append(sensi)
                        CSR_delta_data.append(CSR_info)
                    elif risk_class == 'CSR_REF_Delta':
                        CSR_REF_info = []
                        CSR_REF_bucket = RiskBucketMapping(bucket, CSR_REF_Mapping)
                        label = str(CSR_REF_bucket) + ',' + bucket
                        CSR_REF_info.append(label)
                        CSR_REF_info.append(sensi)
                        CSR_REF_delta_data.append(CSR_REF_info)
                    elif risk_class == 'COM_Delta':
                        commodity_info = []
                        comm_bucket = RiskBucketMapping(bucket, COM_Mapping)
                        label = str(comm_bucket) + ',' + bucket
                        commodity_info.append(label)
                        commodity_info.append(sensi)
                        Commodity_delta_data.append(commodity_info)
                    elif risk_class == 'IR_Vega'or risk_class == "IR_Vega_Inflation":
                        ir_info = []
                        if risk_class == 'IR_Vega_Inflation':
                            risk_type = 'inflation'
                        else:
                            risk_type = ' '
                        risk_class = 'IR_Vega'
                        label = str(bucket) + ',' + risk_type
                        ir_info.append(label)
                        ir_info.append(sensi)
                        IR_vega_data.append(ir_info)
                    elif risk_class == 'FX_Vega':
                        fx_info = []
                        label = bucket
                        fx_info.append(label)
                        fx_info.append(sensi)
                        FX_vega_data.append(fx_info)
                    elif risk_class == 'CSR_REF_Vega':
                        CSR_REF_info = []
                        CSR_REF_bucket = RiskBucketMapping(bucket, CSR_REF_Mapping)
                        label = str(csr_bucket) + ',' + bucket
                        CSR_REF_info.append(label)
                        CSR_REF_info.append(sensi)
                        CSR_REF_vega_data.append(CSR_REF_info)
                    elif risk_class == 'COM_Vega':
                        commodity_info = []
                        comm_bucket = RiskBucketMapping(bucket, COM_Mapping)
                        label = str(comm_bucket) + ',' + bucket
                        commodity_info.append(label)
                        commodity_info.append(sensi)
                        Commodity_vega_data.append(commodity_info)
                    risk_class_list.append(risk_class)
            except ValueError:
                print "Please check %s." % line

    # read hedge sensi files
    if os.path.exists(sensifileName) and not Use_Hedge_Ratio:
        with open(sensifileName) as infile:
            next(infile)  # ignore the first line
            for line in infile:
                line = line.replace(',,', '')
                line = line.replace(',', '')
                contents = line.strip().split(';')
                try:
                    risk_class = contents[0].strip()
                    bucket = contents[1].strip()
                    issuer_parent = contents[3].strip()
                    pillar = contents[4].strip()
                    sensi = contents[5].strip()
                    if float(sensi) <> 0.0:
                        if risk_class == 'IR_Delta_Hedge':
                            ir_info = []
                            risk_type = ' '
                            label = bucket + ',' + risk_type + ',' + pillar + ',' + 'Hedge'
                            ir_info.append(label)
                            ir_info.append(sensi)
                            IR_delta_data.append(ir_info)
                        elif risk_class == 'FX_Delta_Hedge':
                            fx_info = []
                            label = bucket + ',' + 'Hedge'
                            fx_info.append(label)
                            fx_info.append(sensi)
                            FX_delta_data.append(fx_info)
                        elif risk_class == 'CSR_Delta_Hedge':
                            CSR_issuer_weight = IndexMapping(bucket, CSR_Hedge_Index_Composition)
                            NbIssuer = len(CSR_issuer_weight)
                            for i in range(0, NbIssuer):
                                CSR_info = []
                                CSR_issuer = CSR_issuer_weight[i][1]
                                CSR_weight = CSR_issuer_weight[i][2]
                                sensi_issuer = float(sensi) * float(CSR_weight)
                                CSR_bucket_full = RiskBucketMapping(CSR_issuer, CSR_Mapping)
                                if CSR_bucket_full <> 'Not mapped':
                                    contents = CSR_bucket_full.split('_')
                                    if '1a' in contents[0]:
                                        CSR_bucket = 0
                                    elif '1b' in contents[0]:
                                        CSR_bucket = 1
                                    else:
                                        CSR_bucket = int(contents[0])
                                    quality_grade = contents[1]
                                else:
                                    CSR_bucket = 7
                                    quality_grade = 'HY'
                                label = str(CSR_bucket) + ',' + CSR_issuer + ',' + '' + ',' + quality_grade + ',' + pillar + ',' + 'Hedge'
                                CSR_info.append(label)
                                CSR_info.append(sensi_issuer)
                                CSR_delta_data.append(CSR_info)

                except ValueError:
                    print "Please check %s." % line

    # Save sensi np files
    if len(IR_delta_data) > 0:
        title = dir + 'IR_delta_data.npy'
        IR_delta_data = np.array(IR_delta_data)
        np.save(title, IR_delta_data)
    if len(FX_delta_data) > 0:
        title = dir + 'FX_delta_data.npy'
        FX_delta_data = np.array(FX_delta_data)
        np.save(title, FX_delta_data)
    if len(CSR_delta_data) > 0:
        title = dir + 'CSR_delta_data.npy'
        CSR_delta_data = np.array(CSR_delta_data)
        np.save(title, CSR_delta_data)
    if len(CSR_REF_delta_data) > 0:
        title = dir + 'CSR_REF_delta_data.npy'
        CSR_REF_delta_data = np.array(CSR_REF_delta_data)
        np.save(title, CSR_REF_delta_data)
    if len(Commodity_delta_data) > 0:
        title = dir + 'COM_delta_data.npy'
        Commodity_delta_data = np.array(Commodity_delta_data)
        np.save(title, Commodity_delta_data)
    if len(IR_vega_data) > 0:
        title = dir + 'IR_vega_data.npy'
        IR_vega_data = np.array(IR_vega_data)
        np.save(title, IR_vega_data)
    if len(FX_vega_data) > 0:
        title = dir + 'FX_vega_data.npy'
        FX_vega_data = np.array(FX_vega_data)
        np.save(title, FX_vega_data)
    if len(CSR_REF_vega_data) > 0:
        title = dir + 'CSR_REF_vega_data.npy'
        CSR_REF_vega_data = np.array(CSR_REF_vega_data)
        np.save(title, CSR_REF_vega_data)
    if len(Commodity_vega_data) > 0:
        title = dir + 'COM_vega_data.npy'
        Commodity_vega_data = np.array(Commodity_vega_data)
        np.save(title, Commodity_vega_data)
    risk_class_list = np.unique(risk_class_list)
    return risk_class_list

# Read mapping csv file and store as npy file


def read_mapping(filename):
    info = []
    with open(filename) as infile:
        next(infile)  # ignore the first line
        for line in infile:
            contents = line.strip().split(';')
            # remove 0 if the bucket number starts with 0
            if len(contents) > 1:
                try:
                    if contents[1].startswith('0'):
                        contents[1] = contents[1][1:]
                    info.append(contents)
                except ValueError:
                    print "Not a float @ line %s" % line
    info = np.array(info)
    return info
