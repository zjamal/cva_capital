# -*- coding: utf-8 -*-
"""
Created on Thu Feb 01 18:58:34 2018

@author: tding, zjamal
"""

import os
import shutil
from sacva_calculation import calculate_sacva
from write_results_sacva import write_results_sacva
import pandas as pd


# # calculate SACVA results with hedge ratio 1 - temporary function for QIS - to be removed for official upload


# def Calculate_SACVA_Hedge_Ratio_1(Sensi_File, credit_included=True):
#     enable_log = True
#     Riskcharge = 0.0
#     SensiType = []
#     summary_inputs = []
#     CSR_Mapping, COM_Mapping, CSR_REF_Mapping = np.array([]), np.array([]), np.array([])
#     curr_dir = os.path.abspath(os.path.dirname(__file__))
#     #Sensi_File = os.path.join(curr_dir,'input/sensi_input.csv')
#     Hedge_Sensi_File = os.path.join(curr_dir, 'input/hedge_sensi_input.csv')
#     CSR_Hedge_Index_Composition_File = os.path.join(curr_dir, 'input/index_composition.csv')
#     np_dir = os.path.join(curr_dir, 'numpy/')
#     result_dir = os.path.join(curr_dir, 'results/')
#     CSR_Mapping_File = os.path.join(curr_dir, 'input/csr_bucket_mapping.csv')
#     COM_Mapping_File = os.path.join(curr_dir, 'input/com_bucket_mapping.csv')
#     CSR_REF_Mapping_File = os.path.join(curr_dir, 'input/csr_bucket_mapping.csv')
#     # flag to read/not read hedge files
#     # IR_Delta_Hedge,FX_Delta_Hedge,CSR_Delta_Hedge=True,True,True
#     IR_Delta_Hedge, FX_Delta_Hedge, CSR_Delta_Hedge = False, False, False
#     # flag to toggle the logic of specifying hedge ratio vs reading from file
#     Use_Hedge_Ratio = True
#     # Hedge ratio values
#     IR_Delta_Hedge_Ratio, IR_Vega_Hedge_Ratio, FX_Delta_Hedge_Ratio, FX_Vega_Hedge_Ratio, COM_Delta_Hedge_Ratio, COM_Vega_Hedge_Ratio, CSR_Delta_Hedge_Ratio = - \
#         1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0
#     if not(credit_included):
#         CSR_Delta_Hedge_Ratio = -0.0

#     if os.path.exists(CSR_Mapping_File):
#         CSR_Mapping = read_mapping(CSR_Mapping_File)
#     if os.path.exists(COM_Mapping_File):
#         COM_Mapping = read_mapping(COM_Mapping_File)
#     if os.path.exists(CSR_REF_Mapping_File):
#         CSR_REF_Mapping = read_mapping(CSR_REF_Mapping_File)
#     if os.path.exists(CSR_Hedge_Index_Composition_File):
#         CSR_Hedge_Index = read_mapping(CSR_Hedge_Index_Composition_File)
#     SensiType = read_sensi_input(np_dir, Sensi_File, Hedge_Sensi_File, CSR_Mapping, COM_Mapping, CSR_REF_Mapping, Use_Hedge_Ratio)

#     for info in SensiType:
#         if 'Delta' in info:
#             Riskcharge = Calculate_Delta(np_dir, info, enable_log, Use_Hedge_Ratio, IR_Delta_Hedge_Ratio,
#                                          FX_Delta_Hedge_Ratio, COM_Delta_Hedge_Ratio, CSR_Delta_Hedge_Ratio)
#         else:
#             Riskcharge = Calculate_Vega(np_dir, info, enable_log, Use_Hedge_Ratio, IR_Vega_Hedge_Ratio, FX_Vega_Hedge_Ratio, COM_Vega_Hedge_Ratio)
#         output = info + '|' + str(round(Riskcharge, 0))
#         summary_inputs.append(output)

#     WriteSummary(result_dir, summary_inputs)

# calculate SACVA results with hedge ratio 1 - temporary function for QIS - to be removed for official upload

if __name__ == '__main__':
    # Read filenames from folder 'input'
    curr_dir = os.path.abspath(os.path.dirname(__file__))
    sensi_file = os.path.join(curr_dir, 'input/sensi_input.csv')
    hedge_sensi_file = os.path.join(curr_dir, 'input/hedge_sensi_input.csv')
    csr_hedge_index_composition_file = os.path.join(curr_dir, 'input/index_composition.csv')
    csr_mapping_file = os.path.join(curr_dir, 'input/csr_bucket_mapping.csv')
    com_mapping_file = os.path.join(curr_dir, 'input/com_bucket_mapping.csv')
    csr_ref_mapping_file = os.path.join(curr_dir, 'input/csr_bucket_mapping.csv')

    # calculate sacva
    dict_df_data = calculate_sacva(sensi_file, hedge_sensi_file, csr_mapping_file, None, None, csr_hedge_index_composition_file)

    # without hedge file
    # dict_df_data = calculate_sacva(sensi_file, hedge_sensi_file, csr_hedge_index_composition_file, csr_mapping_file)

    # with hedge ratio
    # dict_df_data = calculate_sacva(sensi_file, hedge_sensi_file, csr_hedge_index_composition_file, csr_mapping_file)

    # write results to xls in the 'results' folder
    result_file = os.path.join(curr_dir, 'results/Summary.xls')
    write_results_sacva(dict_df_data, result_file)
