# -*- coding: utf-8 -*-
import os
import sys
import xlwt

def write_results_sacva(summary_input, result_file):
    """Write summary for total risk charge"""
    total_charge = 0.0
    style_title = xlwt.easyxf('font: name Calibri,bold on; align: horiz center;borders: top thin, bottom thin, right thin, left thin')
    style_output = xlwt.easyxf(
        'pattern: pattern solid, fore_colour light_yellow;font: name Calibri; align: horiz left;borders: top thin, bottom thin, right thin, left thin', num_format_str='#,##0')
    wb = xlwt.Workbook()
    ws = wb.add_sheet('Summary')
    ws.write(0, 0, 'Sensi Type', style_title)
    ws.write(0, 1, 'Risk Charge', style_title)
    ws.write(0, 3, ' Total Charge', style_title)
    RowCount = 0
    for info in summary_input:
        RowCount = RowCount + 1
        contents = info.split('|')
        ColCount = 0
        for content in contents:
            if ColCount >= 1:
                ws.write(RowCount, ColCount, float(contents[1]), style_output)
                total_charge = total_charge + float(contents[1])
            else:
                ws.write(RowCount, ColCount, contents[0], style_output)
            ColCount = ColCount + 1
    ws.write(1, 3, total_charge, style_output)

    wb.save(result_file)