import math
import xlwt
import numpy as np
import os
from sacva_parameters import Calculate_Vega_RW, Calculate_WithinBucketCorrelation, Calculate_CrossBucketCorrelation

# Geneal inputs
R = 0.01
m = 1.25


def calculate_vega(dir, AssetClass, enable_log, Use_Hedge_Ratio, hedge_ratio):
    Vega = 0.0
    uniqueinfohedge = []
    uniquedatahedge = []
    # Output excel formatting
    if enable_log:
        wb = xlwt.Workbook()
    style_title = xlwt.easyxf('font: name Calibri,bold on; align: horiz left;borders: top thin, bottom thin, right thin, left thin')
    style_input = xlwt.easyxf(
        'pattern: pattern solid, fore_colour pale_blue;font: name Calibri; align: horiz left;borders: top thin, bottom thin, right thin, left thin', num_format_str='#,##0.0000')
    style_output = xlwt.easyxf(
        'pattern: pattern solid, fore_colour light_yellow;font: name Calibri; align: horiz left;borders: top thin, bottom thin, right thin, left thin', num_format_str='#,##0')
    style_output_4digits = xlwt.easyxf(
        'pattern: pattern solid, fore_colour light_yellow;font: name Calibri; align: horiz left;borders: top thin, bottom thin, right thin, left thin', num_format_str='#,##0.0000')
    style_final = xlwt.easyxf('pattern: pattern solid, fore_colour light_green;'
                              'font: name Calibri,bold on,height 280;'
                              'borders: top thin, bottom thin, right thin, left thin', num_format_str='#,##0.00')
    ######## Asset Class Specific ########
    if AssetClass == 'IR_Vega':
        Vega_Input = ['Bucket Ccy', 'Type']
        title = dir + 'IR_vega_data.npy'
    elif AssetClass == 'FX_Vega':
        Vega_Input = ['Bucket Ccy Pair']
        title = dir + 'FX_vega_data.npy'
    elif AssetClass == 'COM_Vega':
        Vega_Input = ['COM Bucket', 'COM product']
        title = dir + 'COM_vega_data.npy'
    elif AssetClass == 'CSR_REF_Vega':
        Vega_Input = ['CSR_REF Bucket', 'Issuer']
        title = dir + 'CSR_REF_vega_data.npy'

    if os.path.exists(title):
        Viewerdata = np.load(title)
        NbInputs = len(Viewerdata)
        info = []
        if enable_log:
            ws = wb.add_sheet('Vega Input')
            Nb_Vega_Input = len(Vega_Input)
            for i in range(0, Nb_Vega_Input):
                ws.write(0, i, Vega_Input[i], style_title)
            ws.write(0, Nb_Vega_Input, 'Sensi', style_title)
            ws.write(0, Nb_Vega_Input + 1, 'Risk Weight', style_title)
            ws.write(0, Nb_Vega_Input + 2, 'Weighted Sensitivity', style_title)
        for i in range(0, NbInputs):
            try:
                label = Viewerdata[i][0]
                contents = label.split(',')
                NbContents = len(contents)
                if enable_log:
                    for j in range(0, NbContents):
                        ws.write(i + 1, j, contents[j], style_input)
                sensi = float(Viewerdata[i][1])
                RW = Calculate_Vega_RW(AssetClass)
                WS = sensi * RW
                if enable_log:
                    ws.write(i + 1, NbContents, sensi, style_input)
                    ws.write(i + 1, NbContents + 1, RW, style_output_4digits)
                    ws.write(i + 1, NbContents + 2, WS, style_output)
                record = []
                record.append(label)
                record.append(sensi)
                record.append(WS)
                info.append(record)
            except ValueError:
                print "Vega:Please check %s." % str(Viewerdata[i])
        data = np.array(info)
        NbInputs = len(data)
        # Process data further to get unique list of weighted sensitivity
        if NbInputs > 0:
            if enable_log:
                ws = wb.add_sheet('Weighted Sensitivity')
            uniqueInfo = []
            UniqueLabels = np.unique(data[:, 0])
            NbUniqueLabels = len(UniqueLabels)
            if enable_log:
                for i in range(0, Nb_Vega_Input):
                    ws.write(0, i, Vega_Input[i], style_title)
                ws.write(0, Nb_Vega_Input, 'Sensi', style_title)
                ws.write(0, Nb_Vega_Input + 1, 'WS', style_title)
                ws.write(0, Nb_Vega_Input + 2, 'Sensi contribution', style_title)
            if not Use_Hedge_Ratio or AssetClass == 'CSR_REF_Vega':
                for i in range(0, NbUniqueLabels):
                    label = UniqueLabels[i]
                    contribution_detail = data[np.where(data[:, 0] == label)]
                    NbContribution = len(contribution_detail)
                    WS, Sensi_Sum = 0.0, 0.0
                    for j in range(0, NbContribution):
                        sensi_detail = float(contribution_detail[j][1])
                        if enable_log and j < 253 - Nb_Vega_Input:
                            ws.write(2 * i + 1, Nb_Vega_Input + 3 + j, sensi_detail, style_output)
                        Sensi_Sum = Sensi_Sum + sensi_detail
                        ws_detail = float(contribution_detail[j][2])
                        WS = WS + ws_detail
                    label_weightedsensi = ''
                    contents = label.split(',')
                    bucket = contents[0]
                    if enable_log:
                        ws.write(i + 1, 0, bucket, style_input)
                    NbContents = len(contents)
                    for j in range(1, NbContents):
                        if len(label_weightedsensi) > 0:
                            label_weightedsensi = label_weightedsensi + ',' + contents[j]
                        else:
                            label_weightedsensi = contents[j]
                        if enable_log:
                            ws.write(i + 1, j, contents[j], style_input)
                    if enable_log:
                        ws.write(i + 1, NbContents, Sensi_Sum, style_output)
                    if enable_log:
                        ws.write(i + 1, NbContents + 1, WS, style_output)
                    if bucket <> 'Not mapped':
                        unique_content = []
                        unique_content.append(bucket)
                        unique_content.append(label_weightedsensi)
                        unique_content.append(WS)
                        uniqueInfo.append(unique_content)
                uniquedata = np.array(uniqueInfo)
                NbInputs = len(uniquedata)
            else:
                for i in range(0, NbUniqueLabels):
                    label = UniqueLabels[i]
                    contribution_detail = data[np.where(data[:, 0] == label)]
                    NbContribution = len(contribution_detail)
                    WS, Sensi_Sum = 0.0, 0.0
                    for j in range(0, NbContribution):
                        sensi_detail = float(contribution_detail[j][1])
                        if enable_log and j < 253 - Nb_Vega_Input:
                            ws.write(i + 1, Nb_Vega_Input + 3 + j, sensi_detail, style_output)
                        Sensi_Sum = Sensi_Sum + sensi_detail
                        ws_detail = float(contribution_detail[j][2])
                        WS = WS + ws_detail
                    label_weightedsensi = ''
                    contents = label.split(',')
                    bucket = contents[0]
                    ws.write(2 * i + 1, 0, bucket, style_input)
                    ws.write(2 * i + 2, 0, bucket, style_input)
                    NbContents = len(contents)
                    for j in range(1, NbContents):
                        if len(label_weightedsensi) > 0:
                            label_weightedsensi = label_weightedsensi + ',' + contents[j]
                        else:
                            label_weightedsensi = contents[j]
                    ws.write(2 * i + 1, NbContents, 'Net', style_input)
                    ws.write(2 * i + 2, NbContents, 'Hedge', style_input)
                    # Net Sensis
                    ws.write(2 * i + 1, NbContents + 1, Sensi_Sum * (1 + hedge_ratio), style_output)
                    ws.write(2 * i + 1, NbContents + 2, WS * (1 + hedge_ratio), style_output)
                    # Hedge Sensis
                    ws.write(2 * i + 2, NbContents + 1, Sensi_Sum * hedge_ratio, style_output)
                    ws.write(2 * i + 2, NbContents + 2, WS * hedge_ratio, style_output)
                    if bucket <> 'Not mapped':
                        unique_content = []
                        unique_content.append(bucket)
                        unique_content.append(label_weightedsensi)
                        unique_content.append(WS * (1 + hedge_ratio))
                        uniqueInfo.append(unique_content)
                        unique_hedge_content = []
                        unique_hedge_content.append(bucket)
                        unique_hedge_content.append(label_weightedsensi)
                        unique_hedge_content.append(WS * hedge_ratio)
                        uniqueinfohedge.append(unique_hedge_content)
                uniquedatahedge = np.array(uniqueinfohedge)
                uniquedata = np.array(uniqueInfo)
                NbInputs = len(uniquedata)

            if NbInputs > 0:
                # Derive WS*WS and Correlation Matrix for each currency
                Kbs = []
                if enable_log:
                    ws = wb.add_sheet('Risk Bucket')
                RowCount = 0
                # Process Bucket List
                if 'COM' in AssetClass or 'CSR' in AssetClass or 'CSR_REF' in AssetClass:
                    BucketList = map(int, np.unique(uniquedata[:, 0]))
                    BucketList = np.sort(BucketList)
                else:
                    BucketList = np.unique(uniquedata[:, 0])
                NbBuckets = len(BucketList)
                # calculate Kb
                for i in range(0, NbBuckets):
                    bucket = BucketList[i]
                    bucketWS = uniquedata[np.where(uniquedata[:, 0] == str(bucket))]
                    # NbWS = number of components within the bucket
                    NbWS = len(bucketWS)
                    if enable_log:
                        # wirte titles :bucket and WS
                        ws.write(RowCount, 0, bucket, style_title)
                        ws.write(RowCount, 1, 'WS', style_title)
                    Row_cor_matrix = RowCount
                    RowCount = RowCount + 1
                    WS_Square_Sum, Kb = 0.0, 0.0
                    for j in range(0, NbWS):
                        bucket_j = bucketWS[j][0]  # bucket name
                        label_j = bucketWS[j][1]  # component name
                        WS_j = float(bucketWS[j][2])  # WS
                        if enable_log:
                            # list down the components & WS under the bucket
                            ws.write(RowCount, 0, label_j, style_output)
                            ws.write(RowCount, 1, WS_j, style_output)
                            # write the titles for correlaton matrix
                            ws.write(Row_cor_matrix, 3 + j, label_j, style_output)
                        for k in range(0, NbWS):
                            label_k = bucketWS[k][1]
                            WS_k = float(bucketWS[k][2])
                            WS_square = WS_j * WS_k
                            if enable_log:
                                Corr = Calculate_WithinBucketCorrelation(AssetClass, bucket_j + ',' + label_j, bucket_j + ',' + label_k)
                                if enable_log:
                                    ws.write(RowCount, 3 + k, Corr, style_output_4digits)
                                Kb = Kb + WS_square * Corr
                        RowCount = RowCount + 1
                    RowCount = RowCount + 1

                    # calculate hedge component
                    if len(uniquedatahedge) > 0:
                        bucketWShedge = uniquedatahedge[np.where(uniquedatahedge[:, 0] == str(bucket))]
                        NbWShedge = len(bucketWShedge)
                        WS_square_hedge = 0.0
                        if NbWShedge > 0:
                            ws.write(RowCount, 0, "WS(hedge) square sum", style_title)
                            for j in range(0, NbWShedge):
                                WS_square_hedge = WS_square_hedge + float(bucketWShedge[j][2]) * float(bucketWShedge[j][2])
                            ws.write(RowCount, 1, R * WS_square_hedge, style_output_4digits)
                            Kb = Kb + R * WS_square_hedge
                            RowCount = RowCount + 2
                    Kbs.append(math.sqrt(Kb))

            # store consolidated data
                if enable_log:
                    ws = wb.add_sheet('Risk Charge')
                    RowCount = 0
                    ws.write(RowCount, 0, 'Bucket', style_title)
                    ws.write(RowCount, 1, 'Kb', style_title)
                    RowCount = RowCount + 1
                    for i in range(0, NbBuckets):
                        ws.write(RowCount + i, 0, BucketList[i], style_output)
                        ws.write(RowCount + i, 1, Kbs[i], style_output)
                # Save Vega Risk Charge
                    RowCount = RowCount + NbBuckets + 2
                    ColumnCount = 0
                    if enable_log:
                        ws.write(RowCount, ColumnCount, 'Risk Charge Matrix', style_title)
                        ws.write(RowCount + (NbBuckets + 3), ColumnCount, 'Risk Charge Correlation Matrix', style_title)
                    for j in range(0, NbBuckets):
                        bucket_j = BucketList[j]
                        Kb_j = Kbs[j]
                        if enable_log:
                            # x axis titles for Kb^2 matrix
                            ws.write(RowCount + 1, ColumnCount + j + 1, bucket_j, style_title)
                            # y axis titles for Kb^2 matrix
                            ws.write(RowCount + 2 + j, ColumnCount, bucket_j, style_title)
                            # titles for correlation matrix
                            ws.write(RowCount + 1 + (NbBuckets + 3), ColumnCount + j + 1, bucket_j, style_title)
                            ws.write(RowCount + 2 + j + (NbBuckets + 3), ColumnCount, bucket_j, style_title)
                        for k in range(0, NbBuckets):
                            if j != k:
                                bucket_k = BucketList[k]
                                Kb_k = Kbs[k]
                                Gamma = Calculate_CrossBucketCorrelation(AssetClass, bucket_j, bucket_k)
                                result = Gamma * Kb_j * Kb_k
                                if enable_log:
                                    ws.write(RowCount + 2 + j, ColumnCount + k + 1, result, style_output)
                                    ws.write(RowCount + 2 + j + NbBuckets + 3, ColumnCount + k + 1, Gamma, style_output_4digits)
                            else:
                                result = Kb_j * Kb_j
                                if enable_log:
                                    ws.write(RowCount + 2 + j, ColumnCount + k + 1, result, style_output)
                                    ws.write(RowCount + 2 + j + (NbBuckets + 3), ColumnCount + k + 1, 1, style_output)
                            Vega = Vega + result

            # Output the final result
            Vega = math.sqrt(Vega) * m
            curr_dir = os.path.abspath(os.path.dirname(__file__))
            if enable_log:
                ws = wb.add_sheet('Result')
                RowCount = 0
                ws.write(RowCount, 0, 'Vega', style_title)
                ws.write(RowCount, 1, Vega, style_output)
                title = os.path.join(curr_dir, 'results/' + AssetClass + '_output.xls')
                wb.save(title)
    return Vega
