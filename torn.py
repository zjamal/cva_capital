from tornado.wsgi import WSGIContainer
from tornado.ioloop import IOLoop
from tornado.web import FallbackHandler, RequestHandler, Application
from webtool_flask import app

class MainHandler(RequestHandler):
  def get(self):
    self.write("This message passed to Tornado")

tr = WSGIContainer(app)

application = Application([
(r"/tornado", MainHandler),
(r".*", FallbackHandler, dict(fallback=tr)),
])

if __name__ == "__main__":
  application.listen(8000)
  try:
  	IOLoop.instance().start()
  except:
  	IOLoop.instance().stop()